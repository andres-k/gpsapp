﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace GPSApp.Controllers
{
    public class LinesController : Controller
    {
        //
        // GET: /Lines/

        public ActionResult Index()
        {
            //var lines = GetLines();
            //ViewBag.Lines = CreateLineObjects(lines);
            var lines = SortLines(GetLines());
            ViewBag.TrolleybusLines = lines.Where(x => x.Vehicle == 1).OrderBy(x => x.Number).ToList();
            ViewBag.BusLines = lines.Where(x => x.Vehicle == 2).OrderBy(x => x.Number).ToList();
            ViewBag.TramLines = lines.Where(x => x.Vehicle == 3).OrderBy(x => x.Number).ToList();

            return View();
        }

        private List<GPSApp.Models.Line> GetLines()
        {
            var req = HttpWebRequest.Create("http://soiduplaan.tallinn.ee/gps.txt");
            using (var resp = req.GetResponse())
            {
                string line;
                List<GPSApp.Models.Line> lines = new List<GPSApp.Models.Line> { };
                var stream = new StreamReader(resp.GetResponseStream());
                while ((line = stream.ReadLine()) != null)
                {
                    lines.Add(new GPSApp.Models.Line(line.Split(',')[1], Convert.ToInt32(line.Split(',')[0])));
                }
                return lines;
            }
        }

        private List<GPSApp.Models.Line> SortLines(List<GPSApp.Models.Line> lines)
        {
            var FirstSort = lines.Where(x => x.Number != "0").ToList();
            var SecondSort = FirstSort.GroupBy(x => x.Number).Select(y => y.First()).ToList();
            return SecondSort;
        }

    }
}
