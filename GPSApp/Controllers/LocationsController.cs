﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace GPSApp.Controllers
{
    public class LocationsController : Controller
    {
        //
        // GET: /Locations/

        public ActionResult Show(string number, string vehicle)
        {
            ViewBag.Locations = GetLocations(number, vehicle);
            ViewBag.Line = new GPSApp.Models.Line(number, Convert.ToInt32(vehicle));

            return View();
        }

        private List<GPSApp.Models.Location> GetLocations(string number, string vehicle)
        {
            var req = HttpWebRequest.Create("http://soiduplaan.tallinn.ee/gps.txt");
            using (var resp = req.GetResponse())
            {
                string line;
                string[] SplitLine;
                List<GPSApp.Models.Location> locations = new List<GPSApp.Models.Location> { };
                var stream = new StreamReader(resp.GetResponseStream());
                while ((line = stream.ReadLine()) != null)
                {
                    SplitLine = line.Split(',');
                    if(SplitLine[1].Equals(number) && SplitLine[0].Equals(vehicle))
                    {
                        locations.Add(new GPSApp.Models.Location(line.Split(',')[2], line.Split(',')[3]));
                    }
                }
                return locations;
            }
        }



    }
}
