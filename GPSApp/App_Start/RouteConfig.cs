﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace GPSApp
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{number}/{vehicle}",
                defaults: new { controller = "Lines", action = "Index", 
                                number = UrlParameter.Optional, vehicle = UrlParameter.Optional }
            );
        }
    }
}