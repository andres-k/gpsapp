﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GPSApp.Models
{
    public class Location
    {
        public string Longitude { get; set; }
        public string Latitude { get; set; }
        public string DecLng { 
            get { return ToDecimal(Longitude); } 
        }
        public string DecLat { 
            get { return ToDecimal(Latitude); } 
        }

    
        
        public Location(string longitude, string latitude)
        {
            Longitude = longitude;
            Latitude = latitude;
        }

        public string ToDecimal(string coordinate)
        {
            return coordinate.Insert(2, ".");
        }
        
    }
}