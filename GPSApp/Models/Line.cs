﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GPSApp.Models
{
    public class Line
    {
        public string Number { get; set; }
        public int Vehicle { 
            get; set;   // 3 - tram, 2- bus, 1 - trolleybus
        
        }
        public Line(string number, int vehicle)
        {
            Number = number;
            Vehicle = vehicle;
        }

        public string VehicleName()
        {
            switch( Vehicle )
            {
                case 1:
                    return "trolleybus";
                case 2:
                    return "bus";
                case 3:
                    return "tram";
                default:
                    return "";
            }

        }
    }
}